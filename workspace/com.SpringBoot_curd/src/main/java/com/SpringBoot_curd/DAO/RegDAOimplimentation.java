package com.SpringBoot_curd.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.SpringBoot_curd.VO.RegVO;

@Repository
public class RegDAOimplimentation implements RegDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void insert(RegVO regVO) {
		try {
			Session session=this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(regVO);
			

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
		
		
	}

	public List search() {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from RegVO where status="+true);
		
		List ls=q.list();
		return ls;
	}

	@Override
	public List searchbyid(RegVO regVO) {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from RegVO where status=true and id="+regVO.getId());
		
		
		return q.list();
	}

	

	
	
}
