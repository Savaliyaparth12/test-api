package com.SpringBoot_curd.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.SpringBoot_curd.Service.RegService;
import com.SpringBoot_curd.Service.RegServiceImplimentation;
import com.SpringBoot_curd.VO.RegVO;

@Controller
public class RegController {
	
	@Autowired 
	RegService r1;
	
	

	@RequestMapping(value="/")
	public ModelAndView load(@ModelAttribute RegVO regVO)
	{
		
		return new ModelAndView("Register","data",new RegVO());
		
	}
	
	@RequestMapping(value="insert.html",method=RequestMethod.POST)
	public ModelAndView Insert(@ModelAttribute RegVO regVO)
	{
		regVO.setStatus(true);
		System.out.println(regVO);
		this.r1.insert(regVO);
		return new ModelAndView("redirect:/");
		
		
	}
	
	@RequestMapping(value="search.html",method=RequestMethod.GET)
	public ModelAndView Search()
	{
		List searchList=this.r1.search();
		return new ModelAndView("Search","key",searchList);
		
	}
	
	@RequestMapping(value="delete",method=RequestMethod.GET)
	public ModelAndView Delete(@RequestParam int id,@ModelAttribute RegVO regVO)
	{
		regVO.setId(id);
		
		List ls=r1.searchbyid(regVO);
		RegVO v2=(RegVO)ls.get(0);
		v2.setStatus(false);
		
		this.r1.insert(v2);
		
		return new ModelAndView("redirect:search.html");
		
		
	}
	
	@RequestMapping(value="edit",method=RequestMethod.GET)
	public ModelAndView Edit(@RequestParam int id,@ModelAttribute RegVO regVO)
	{
		regVO.setId(id);
		
		List ls=r1.searchbyid(regVO);
		
		return new ModelAndView("Register","data",(RegVO)ls.get(0));
		
		
	}
}
