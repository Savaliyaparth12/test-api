package com.SpringBoot_curd.Service;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.SpringBoot_curd.DAO.RegDAO;
import com.SpringBoot_curd.VO.RegVO;


@Service
public class RegServiceImplimentation implements RegService{

	@Autowired
	RegDAO d1;
	
	@Override
	@Transactional

	public void insert(RegVO regVO) {
		
	this.d1.insert(regVO);
	}

	@Override
	public List search() {
		
		
		return this.d1.search();

	}

	@Override
	public List searchbyid(RegVO regVO) {
		return this.d1.searchbyid(regVO);
	}


	


}
