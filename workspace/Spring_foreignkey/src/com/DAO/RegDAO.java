package com.DAO;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;


import com.VO.LoginVO;
import com.VO.RegVO;
@Repository
public class RegDAO {


	@Autowired
	SessionFactory sessionFactory;
	
	public void insertloign(LoginVO loginVO) {
		// TODO Auto-generated method stub
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.saveOrUpdate(loginVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
	}
	
	public void insertregister(RegVO regVO) {
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.saveOrUpdate(regVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
	}

	public List search() {
		List searchList=new ArrayList<>();
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			Query q =session.createQuery("from RegVO");
			searchList=q.list();
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		return searchList;
		
	}

	public void deletelogin(LoginVO loginVO) {
		
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.delete(loginVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
	}

	public void deletergister(RegVO regVO) {
	
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.delete(regVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
	}

	public List edit(RegVO regVo) {
		
		List ls=new ArrayList();
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			Query q=session.createQuery("from RegVO where id="+regVo.getId());
		    ls=q.list();
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
    return ls;	
		
	}

	
	

}
