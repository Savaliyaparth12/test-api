package com.Controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sun.research.ws.wadl.Request;

@Controller

public class DemoController 
{
	
	String s  ="this is spring examplle";
	
	
	@RequestMapping(value="Register.html",method=RequestMethod.GET)
	
	public ModelAndView load()
	{
		return new ModelAndView("demo","msg",s);
	}
	
	@RequestMapping(value="save.html",method=RequestMethod.POST)
	
	public ModelAndView insert(HttpServletRequest request,Model model)
	{
		String s1=request.getParameter("fn");
		String s2=request.getParameter("ln");
		String s3=request.getParameter("un");
		String s4=request.getParameter("pw");
		
		HttpSession hs=request.getSession();
		hs.setAttribute("rfn", s1);
		hs.setAttribute("rln", s2);
		hs.setAttribute("run", s3);
		hs.setAttribute("rpw", s4);
		
		model.addAttribute("fn",s1);
		model.addAttribute("ln",s2);
		model.addAttribute("un",s3);
		model.addAttribute("pw",s4);
		
		
/*		return new ModelAndView("print","key",model);
*/		return new ModelAndView("login","key",model);
		
	}
	
	@RequestMapping(value="verify.html",method=RequestMethod.GET)
	public ModelAndView verify(HttpServletRequest request,Model model)
	{
		HttpSession hs=request.getSession();
		String rfn1=(String)hs.getAttribute("rfn");
		String rln1=(String)hs.getAttribute("rln");
		String run1=(String)hs.getAttribute("run");
		String rpw1=(String)hs.getAttribute("rpw");
		
		String s33=request.getParameter("un1");
		String s44=request.getParameter("pw1");
		
		
		
		
		if(s33.equals(run1) && s44.equals(rpw1))
		{
			model.addAttribute("fn",rfn1);
			model.addAttribute("ln",rln1);
			return new ModelAndView("print","k",model);
		}
		else
		{
			return new ModelAndView("login","k1",model);
		}
		
		
	}
	

}
