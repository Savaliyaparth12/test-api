package com.Country_state.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.Country_state.Service.CountryService;
import com.Country_state.VO.CountryVO;

@Controller
public class CountryController {

	@Autowired
	CountryService c1;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView load() {

		return new ModelAndView("Menu", "data", new CountryVO());

	}

	@RequestMapping(value = "load", method = RequestMethod.GET)
	public ModelAndView loadCountry() {

		return new ModelAndView("Country", "data", new CountryVO());

	}

	@RequestMapping(value = "insert", method = RequestMethod.POST)
	public ModelAndView InsertCountry(@ModelAttribute CountryVO countryVO) {

		this.c1.insert(countryVO);

		return new ModelAndView("redirect:/");

	}

	@RequestMapping(value = "search", method = RequestMethod.GET)
	public ModelAndView Search(@ModelAttribute CountryVO countryVO) {
		List searchList = new ArrayList();
		searchList = this.c1.search();
		return new ModelAndView("SearchCountry","SearchList",searchList);
	}
}
