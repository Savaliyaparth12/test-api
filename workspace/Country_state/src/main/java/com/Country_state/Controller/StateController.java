package com.Country_state.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.Country_state.Service.CountryService;
import com.Country_state.Service.StateService;
import com.Country_state.VO.CountryVO;
import com.Country_state.VO.StateVO;


@Controller
public class StateController {

	@Autowired
	StateService s1;

	@Autowired
	CountryService c1;

	@RequestMapping(value = "loadstate", method = RequestMethod.GET)
	public ModelAndView loadstate() {
		List ls = c1.search();
		return new ModelAndView("State", "key", new StateVO()).addObject("countrylist", ls);

	}

	@RequestMapping(value = "insertstate", method = RequestMethod.POST)
	public ModelAndView Insert(@ModelAttribute StateVO stateVO) {

		this.s1.insertstate(stateVO);
		return new ModelAndView("redirect:/loadstate");

	}
	
	@RequestMapping(value="searchstate",method=RequestMethod.GET)
    public ModelAndView Search(@ModelAttribute StateVO stateVO)
    {
    	List ls=this.s1.search();
    	
    	System.out.println(ls.size());
    	
		return new ModelAndView("SearchState","SearchList", ls);
    }
	
	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public ModelAndView Delete(@RequestParam int id, @ModelAttribute StateVO stateVO) {
		stateVO.setSid(id);

		List ls = s1.searchbyid(stateVO);
		StateVO v2 = (StateVO) ls.get(0);
		v2.setStatus(false);

		this.s1.insertstate(v2);

		return new ModelAndView("redirect:/");

	}
	
	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView Edit(@RequestParam int id, @ModelAttribute StateVO stateVO) {
		stateVO.setSid(id);

		List ls = s1.searchbyid(stateVO);
		List ls1 = c1.search();

		return new ModelAndView("State", "key", (StateVO) ls.get(0)).addObject("countrylist", ls1);

	}

}
