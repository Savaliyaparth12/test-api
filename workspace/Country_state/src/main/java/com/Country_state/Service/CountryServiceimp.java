package com.Country_state.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Country_state.DAO.CountryDAO;
import com.Country_state.VO.CountryVO;


@Service
@Transactional
public class CountryServiceimp implements CountryService{
	
	@Autowired
	CountryDAO d1;

	public void insert(CountryVO countryVO) {
		this.d1.insert(countryVO);

	}

	@Override
	public List search() {
		return this.d1.search();
	}
	
	

}
