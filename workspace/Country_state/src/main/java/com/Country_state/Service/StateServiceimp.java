package com.Country_state.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Country_state.DAO.CountryDAO;
import com.Country_state.DAO.StateDAO;
import com.Country_state.VO.CountryVO;



import com.Country_state.VO.StateVO;
@Service
@Transactional
public class StateServiceimp implements StateService{
	
	@Autowired
	StateDAO d2;
	
	@Override
	public void insertstate(StateVO stateVO) {
		this.d2.insertstate(stateVO);
	}

	@Override
	public List search() {
		// TODO Auto-generated method stub
		return this.d2.search();
	}

	@Override
	public List searchbyid(StateVO stateVO) {
		return this.d2.searchbyid(stateVO);
	}

}
