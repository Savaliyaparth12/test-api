package com.Country_state.DAO;

import java.util.List;

import com.Country_state.VO.StateVO;

public interface StateDAO {

	public void insertstate(StateVO stateVO);

	public List search();

	public List searchbyid(StateVO stateVO);

}
