package com.Country_state.DAO;

import org.hibernate.Session;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.Country_state.VO.CountryVO;

@Repository
public class CountryDAOimp implements CountryDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insert(CountryVO countryVO) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(countryVO);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
	}

	@Override
	public List search() {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from CountryVO where status="+true);
		
		List ls=q.list();
		return ls;
	}

}
