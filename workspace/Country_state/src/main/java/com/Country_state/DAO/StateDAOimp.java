package com.Country_state.DAO;
import org.hibernate.Session;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.Country_state.VO.CountryVO;
import com.Country_state.VO.StateVO;

@Repository
public class StateDAOimp implements StateDAO
{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void insertstate(StateVO stateVO) {
		// TODO Auto-generated method stub
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(stateVO);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
		
	}

	@Override
	public List search() {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from StateVO where status="+true);
		
		List ls=q.list();
		return ls;
	}

	@Override
	public List searchbyid(StateVO stateVO) {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from StateVO where status=true and id="+stateVO.getSid());
		
		
		return q.list();
	}

}
