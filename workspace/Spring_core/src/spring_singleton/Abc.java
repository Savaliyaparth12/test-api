package spring_singleton;

public class Abc 
{
	
	private static Abc a1;
	
	public static Abc getobj()
	{
		if(a1==null)
		{
			a1=new Abc();
			return a1;
			
		}
		else {
			return a1;
		}
	}
	
}
