<%-- <%@page import="org.apache.jasper.compiler.Node.UseBean"%> --%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
/* 	Student s=new Student();
	s.setFn("savaliya"); //set name <!-- setter getter mathod by class -->
	s.setLn("parth");
	String a=s.getFn(); //get name 
	String a1=s.getLn();
	
	out.println(a);
	out.println(a1);
	 */
	
%>

<jsp:useBean id="obj" class="p.Student"></jsp:useBean>
<jsp:setProperty property="fn" name="obj" value="hello"/>  <!-- setter getter mathod by useBean -->
<jsp:setProperty property="ln" name="obj" value="parth"/>
<jsp:getProperty property="fn" name="obj"/>
<jsp:getProperty property="ln" name="obj"/>

</body>
</html>