package com.foreignkey.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foreignkey.DAO.RegDAO;
import com.foreignkey.VO.LoginVO;
import com.foreignkey.VO.RegVO;

@Service
@Transactional
public class RegServiceImp implements RegService {

	@Autowired
	RegDAO d1;

	@Override
	public void insertregister(RegVO regVO) {
		this.d1.insertregister(regVO);

	}

	@Override
	public void insertlogin(LoginVO loginVO) {
		this.d1.insertlogin(loginVO);
	}

	@Override
	public List search() {
		
		return this.d1.search();
	}

	@Override
	public List searchbyid(RegVO regVO) {
		
		return this.d1.searchbyid(regVO);
	}


}
