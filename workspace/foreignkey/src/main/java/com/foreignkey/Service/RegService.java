package com.foreignkey.Service;

import java.util.List;

import com.foreignkey.VO.LoginVO;
import com.foreignkey.VO.RegVO;

public interface RegService {

	public void insertregister(RegVO regVO);

	public void insertlogin(LoginVO loginVO);

	public List search();

	public List searchbyid(RegVO regVO);

}
