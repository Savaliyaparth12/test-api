package com.foreignkey.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.foreignkey.Service.RegService;
import com.foreignkey.VO.LoginVO;
import com.foreignkey.VO.RegVO;

@Controller
public class RegController {

	@Autowired
	RegService r1;

	@RequestMapping(value = "/")
	public ModelAndView load(@ModelAttribute RegVO regVO) {
		System.out.println("print");
		return new ModelAndView("Register", "data", new RegVO());

	}

	@RequestMapping(value = "insert", method = RequestMethod.POST)
	public ModelAndView InsertRegister(@ModelAttribute RegVO regVO) {
		regVO.setStatus(true);
		LoginVO loginVO = regVO.getLoginvo();
		this.r1.insertlogin(loginVO);

		this.r1.insertregister(regVO);
		return new ModelAndView("redirect:/");

	}

	@RequestMapping(value = "search", method = RequestMethod.GET)
	public ModelAndView Search(@ModelAttribute RegVO regVO) {
		List searchList = new ArrayList();
		searchList = r1.search();
		return new ModelAndView("Search", "SearchList", searchList);
	}

	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public ModelAndView Delete(@RequestParam int id, @ModelAttribute RegVO regVO) {
		regVO.setId(id);

		List ls = r1.searchbyid(regVO);
		RegVO v2 = (RegVO) ls.get(0);
		v2.setStatus(false);

		this.r1.insertregister(v2);

		return new ModelAndView("redirect:/");

	}

	@RequestMapping(value = "edit", method = RequestMethod.GET)
	public ModelAndView Edit(@RequestParam int id, @ModelAttribute RegVO regVO) {
		regVO.setId(id);

		List ls = r1.searchbyid(regVO);

		return new ModelAndView("Register", "data", (RegVO) ls.get(0));

	}
}
