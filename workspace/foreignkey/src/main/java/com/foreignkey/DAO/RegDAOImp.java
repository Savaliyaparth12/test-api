package com.foreignkey.DAO;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.foreignkey.VO.LoginVO;
import com.foreignkey.VO.RegVO;


@Repository
public class RegDAOImp implements RegDAO{

	@Autowired
	private SessionFactory sessionFactory;

	public void insertregister(RegVO regVO) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(regVO);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}

	}

	@Override
	public void insertlogin(LoginVO loginVO) {
		try {
			Session session = this.sessionFactory.getCurrentSession();
			session.saveOrUpdate(loginVO);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
		
	}

	@Override
	public List search() {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from RegVO where status="+true);
		
		List ls=q.list();
		return ls;
	}

	@Override
	public List searchbyid(RegVO regVO) {
		Session session=this.sessionFactory.openSession();
		Query q =session.createQuery("from RegVO where status=true and id="+regVO.getId());
		
		
		return q.list();
	}
}
