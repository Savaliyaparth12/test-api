package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.RegDAO;
import VO.RegVO;

/**
 * Servlet implementation class RegController
 */
@WebServlet("/RegController")
public class RegController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();	
		String flag=request.getParameter("flag");
		
		String s=request.getParameter("fn");
		String s1=request.getParameter("ln");
		
		if(flag.equals("insert"))
		{
			RegVO v=new RegVO();
			v.setFn(s);
			v.setLn(s1);
			
			RegDAO d=new RegDAO();
			d.insert(v);
		}
		

		if(flag.equals("search"))
		{
		
			
			RegDAO d=new RegDAO();
			List ls=d.search();
			System.out.println(ls.size());
			
			HttpSession hs=request.getSession();
			hs.setAttribute("data", ls);
/*			String r=(String)hs.getAttribute("data");
*/			
			
			response.sendRedirect("mvc/search.jsp");
			
		}
		
		if(flag.equals("delete"))
		{
			int i1=Integer.parseInt(request.getParameter("id"));
			RegVO v=new RegVO();
			v.setId(i1);
			RegDAO d=new RegDAO();
			d.delete(v);
			
			List ls=d.search();
			System.out.println(ls.size());
			
			HttpSession hs=request.getSession();
			hs.setAttribute("data", ls);
			
			response.sendRedirect("mvc/search.jsp");

		}
		
		if(flag.equals("edit"))
		{
			int i1=Integer.parseInt(request.getParameter("id"));
			RegVO v=new RegVO();
			v.setId(i1);
			RegDAO d=new RegDAO();
			
			List ls=d.edit(v);
			System.out.println(ls.size());
			
			HttpSession hs=request.getSession();
			hs.setAttribute("data", ls);
			
			response.sendRedirect("mvc/update.jsp");
		}
		
		if(flag.equals("update"))
		{
			int i1=Integer.parseInt(request.getParameter("id1"));
			String fn = request.getParameter("fn1");
			String ln = request.getParameter("ln1");
			
			RegVO v=new RegVO();

			v.setFn(fn);
			v.setId(i1);
			v.setLn(ln);
			RegDAO d=new RegDAO();
			d.update(v);
			
		}
	
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
