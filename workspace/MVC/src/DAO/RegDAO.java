package DAO;
import java.sql.*;
import java.util.*;
import java.io.*;
import VO.RegVO;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.catalina.connector.Request;

public class RegDAO 
{
	
	public void insert(RegVO v)
	{
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			st.executeUpdate("insert into forms(fn,ln) values ('"+v.getFn()+"','"+v.getLn()+"')");
			System.out.println("done");
			st.close();
			c.close();
		}
		catch(Exception e)
		{
			System.out.println(e);

		}
	}
	
	public List search()
	{
		List ls=new ArrayList();
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			ResultSet r=st.executeQuery("Select * from forms");
			
			while(r.next())
			{
				RegVO v=new RegVO();
				v.setId(r.getInt("id"));
				v.setFn(r.getString("fn"));
				v.setLn(r.getString("ln"));
				ls.add(v);
				 
				
			}
			
			
		}
		
		catch(Exception e)
		{
			System.out.println(e);

		}
		return ls;
			
	}
	
	public void delete(RegVO v)
	{
		try
		{
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			st.executeUpdate("delete from forms where id="+v.getId());
			st.close();
			c.close();
			
			
		}
		catch (Exception e) 
		{
			System.out.println(e);
		}
		
	}
	
	public  List edit(RegVO v)
	{
		List ls=new ArrayList();
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			ResultSet r=st.executeQuery("Select * from forms where id="+v.getId());
			
			while(r.next())
			{
				v.setId(r.getInt("id"));
				v.setFn(r.getString("fn"));
				v.setLn(r.getString("ln"));
				ls.add(v);
				
			}
			
			
		}
		
		catch(Exception e)
		{
			System.out.println(e);

		}
		return ls;
			
	}
	
	public void update(RegVO v)
	{
		
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			st.executeUpdate("update forms set fn = '"+v.getFn()+"',ln='"+v.getLn()+"' where id='"+v.getId()+"'");
			st.close();
			c.close();
	    } 
		catch (Exception e) 
		{
			System.out.println(e);
		}
	}
	
	
}
