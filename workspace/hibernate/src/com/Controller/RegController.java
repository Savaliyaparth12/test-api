package com.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.DAO.RegDAO;
import com.VO.RegVO;

/**
 * Servlet implementation class RegController
 */
@WebServlet("/RegController")
public class RegController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String flag=request.getParameter("flag");
		if(flag.equals("search"))
		{	
			search(request,response);
			response.sendRedirect("Search.jsp");
		}
		
		
		if(flag.equals("delete"))
		{	
			delete(request,response);
			search(request,response);
			response.sendRedirect("Search.jsp");
		
		}
		
		if(flag.equals("edit"))
		{	
			edit(request,response);
			search(request,response);
			response.sendRedirect("update.jsp");
		
		}
		if(flag.equals("update"))
		{	
			update(request,response);
			search(request,response);
			response.sendRedirect("Search.jsp");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String flag=request.getParameter("flag");
		if(flag.equals("insert"))
		{	
			insert(request,response);
			response.sendRedirect("Register.jsp");
		}
		

		
	}
		
		protected void insert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			String Fn = request.getParameter("firstName");
			String Ln = request.getParameter("lastName");
			
			RegVO regVO = new RegVO();
			regVO.setFristName(Fn);
			regVO.setLastName(Ln);
			
			RegDAO regDAO = new RegDAO();
			regDAO.insert(regVO);

			
		}
		
		protected void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			
			List SearchList =new ArrayList<>();
			
			System.out.println("search controlleerrrrrr");
			
			RegDAO regDAO = new RegDAO();
			SearchList =  regDAO.search();
			
			System.out.println(SearchList.size());
			
			HttpSession session = request.getSession();
			session.setAttribute("SearchList", SearchList);
					
			
		}
		
		protected void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			String flag=request.getParameter("flag");
			if(flag.equals("delete"))
			{	
				
				int id=Integer.parseInt(request.getParameter("id"));
				RegVO regVO=new RegVO();
				regVO.setId(id);
				System.out.println(id);
				
				RegDAO regDAO = new RegDAO();
				regDAO.delete(regVO);
				
				
			}
			
			
		}
		
		protected void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			String flag=request.getParameter("flag");
			if(flag.equals("edit"))
			{	
				
				int id=Integer.parseInt(request.getParameter("id"));
				RegVO regVO=new RegVO();
				regVO.setId(id);
				
				RegDAO regDAO = new RegDAO();
			    List listsearch=regDAO.edit(regVO);
				
			    HttpSession hs=request.getSession();
				hs.setAttribute("listsearch", listsearch);
				
				
			}
		}
		
		protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			int i1=Integer.parseInt(request.getParameter("id1"));
			String fn = request.getParameter("fn1");
			String ln = request.getParameter("ln1");
			
			RegVO regVO=new RegVO();
			regVO.setFristName(fn);
			regVO.setId(i1);
			regVO.setLastName(ln);
			
			RegDAO regDAO=new RegDAO();
			regDAO.update(regVO);
			
		}
			
	}


