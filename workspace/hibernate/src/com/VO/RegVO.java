package com.VO;
import javax.persistence.*;

@Entity
@Table(name="Hb_Reg")
public class RegVO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="RegId")
	private int Id;
	
	@Column(name="frisrtName")
	private String fristName;

	@Column(name="lastName")
	private String lastName;

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	
	
}
