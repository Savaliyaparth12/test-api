package fdb;

import java.io.*;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 * Servlet implementation class form1
 */
@WebServlet("/form1")
public class form1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public form1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
			response.setContentType("text/html");
			PrintWriter out=response.getWriter();
			
			String flag=request.getParameter("flag");
			
			if(flag.equals("insert"))
			{
				try
				{
					String s=request.getParameter("fn");
					String s1=request.getParameter("ln");
					String s2=request.getParameter("un");
					String s3=request.getParameter("pw");
					Class.forName("com.mysql.jdbc.Driver");
					Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
					Statement st=c.createStatement();
					st.executeUpdate("insert into forms(fn,ln,username,password) values ('"+s+"','"+s1+"','"+s2+"','"+s3+"')");
					out.println("done");
					st.close();
					c.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
	
				}
			}
			
			if(flag.equals("search"))
			{
				search(request,response);
			}
			
			if(flag.equals("delete"))
			{
				try
				{
					int i1=Integer.parseInt(request.getParameter("id"));
					Class.forName("com.mysql.jdbc.Driver");
					Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
					Statement st=c.createStatement();
					st.executeUpdate("delete from forms where id="+i1);
					st.close();
					c.close();
					search(request,response);
					
				}
				catch (Exception e) 
				{
					
				}
			}
			
			if(flag.equals("update"))
			{
				String k=request.getParameter("fn");
				String k1=request.getParameter("ln");
				String k2=request.getParameter("un");
				String k3=request.getParameter("pw");
				String k25=request.getParameter("id");


				try
				{
					Class.forName("com.mysql.jdbc.Driver");
					Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
					Statement st=c.createStatement();
					st.executeUpdate("update forms set fn = '"+k+"',ln='"+k1+"',username='"+k2+"',password='"+k3+"' where id='"+k25+"'");
					
					st.close();
					c.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();	

				}
			}
			
			
			
				
			
	}
	
	protected void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		try 
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			ResultSet r=st.executeQuery("Select* from forms ");

			out.println("<table border=2>");
			out.println("<th>id</th>");
			out.println("<th>fn</th>");
			out.println("<th>ln</th>");
			out.println("<th>un</th>");
			out.println("<th>ps</th>");
			out.println("<th>delete</th>");
			out.println("<th>update</th>");
			
			while(r.next())
			{
				int i=r.getInt("id");
				String f=r.getString("fn");
				String l=r.getString("ln");
				String u=r.getString("username");
				String p=r.getString("password");
				
				
				out.println("<tr><td>"+i+"</td>");
				out.println("<td>"+f+"</td>");
				out.println("<td>"+l+"</td>");
				out.println("<td>"+u+"</td>");
				out.println("<td>"+p+"</td>");
				out.println("<td><a href=deletedb1?x="+i+">delete</a></td>");
				out.println("<td><a href=update?x="+i+">update</a></td></tr>");
			
			}
			out.println("</table>");

			st.close();
			c.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	protected void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		try 
		{
			int i=Integer.parseInt(request.getParameter("x"));	
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			ResultSet r=st.executeQuery("Select * from forms");
			
			String fn="",ln="",un="",pw="";
			int id=0;
				
			while(r.next())
			{
				fn=r.getString("fn");
				ln=r.getString("ln");
				un=r.getString("username");
				pw=r.getString("password");
				
				
				id=r.getInt("id");
				
				
				
			}

			out.println("<form action='update1' method='get'>");
			out.println("First Nmae:<input type='text' name='fn' value='"+fn+"'><br><br>");
			out.println("Last Name:<input type='text' name='ln' value='"+ln+"'><br><br>");
			out.println("username:<input type='text' name='un' value='"+un+"'><br><br>");
			out.println("password:<input type='text' name='pw' value='"+pw+"'><br><br>");
			out.println("<input type='hidden' name='id' value='"+id+"'><br>");
			out.println("<input type='submit'>");
			out.println("</form>");
			r.close();
			st.close();
			c.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
}		
