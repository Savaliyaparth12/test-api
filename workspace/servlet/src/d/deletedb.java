package d;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class deletedb
 */
@WebServlet("/deletedb")
public class deletedb extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public deletedb() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection c=DriverManager.getConnection("jdbc:mysql://localhost/demodb","root","root");
			Statement st=c.createStatement();
			ResultSet r=st.executeQuery("select * from forms");
			
			
			
			out.println("<table border=2>");
			out.println("<th>id</th>");
			out.println("<th>fn</th>");
			out.println("<th>ln</th>");
			out.println("<th>un</th>");
			out.println("<th>ps</th>");
			out.println("<th>delete</th>");
			out.println("<th>update</th>");
			
			while(r.next())
			{
				int i=r.getInt("id");
				String f=r.getString("fn");
				String l=r.getString("ln");
				String u=r.getString("username");
				String p=r.getString("password");
				
				
				out.println("<tr><td>"+i+"</td>");
				out.println("<td>"+f+"</td>");
				out.println("<td>"+l+"</td>");
				out.println("<td>"+u+"</td>");
				out.println("<td>"+p+"</td>");
				out.println("<td><a href=deletedb1?x="+i+">delete</a></td>");
				out.println("<td><a href=update?x="+i+">update</a></td></tr>");
			
			}
			out.println("</table>");

			st.close();
			c.close();
			

		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
