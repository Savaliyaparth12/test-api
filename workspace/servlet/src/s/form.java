package s;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class form
 */
@WebServlet("/form")
public class form extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public form() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		// TODO Auto-generated method stub
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		String s=request.getParameter("fn");
		String s1=request.getParameter("ln");
		String s2=request.getParameter("un");
		String s3=request.getParameter("pw");
		
		HttpSession se=request.getSession();
		se.setAttribute("fn", s);
		se.setAttribute("ln", s1);
		se.setAttribute("un", s2);
		se.setAttribute("pw", s3);

		response.sendRedirect("form/login.jsp");
		
		
		
		
	}

}
