package com.Controller;
import com.DAO.CountryDAO;
import com.DAO.StateDAO;
import com.VO.CountryVO;
import com.VO.StateVO;
import com.sun.research.ws.wadl.Request;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class StateController {

	@Autowired
	StateDAO d2;
	@Autowired
	CountryDAO d3;
	
	@RequestMapping(value="loadstate.html" ,method=RequestMethod.GET)
	public ModelAndView loadCountry()
	{
 	   List ls=d3.search();//search ae country search  ni method chhe 
 	   return new ModelAndView("State","key",new StateVO()).addObject("countrylist",ls);	
	
	}
	
	@RequestMapping(value="insertstate.html",method=RequestMethod.POST)
	public ModelAndView Insert(@ModelAttribute StateVO stateVO)
	{
		
		d2.insert(stateVO);
		/*return new ModelAndView("redirect:/loadstate.html");*/
		return new ModelAndView("redirect:/searchstate.html");

	}
	
	@RequestMapping(value="searchstate.html",method=RequestMethod.GET)
    public ModelAndView Search(@ModelAttribute CountryVO countryVO)
    {
    	List searchList=new ArrayList<>();
    	searchList=d2.search();//search ae state search krvani method chhe 
		return new ModelAndView("SearchState","SearchList", searchList);
    }
	
	 @RequestMapping(value="delete.html",method=RequestMethod.GET)
		public ModelAndView Delete(@ModelAttribute StateVO stateVO,@RequestParam int id)//id ae delete ni link ma aapelu chhe ae
		{
	    	stateVO.setSid(id);
	    	d2.delete(stateVO);
			return new ModelAndView("redirect:/searchstate.html");
		}
	 
	 @RequestMapping(value="edit.html",method=RequestMethod.GET)
	   	public ModelAndView Edit(@ModelAttribute StateVO stateVO,@RequestParam int id)
	   	{
		 List ls=d3.search();//search ae country search  ni method chhe 

		 	
	    	stateVO.setSid(id);
			List lss=d2.edit(stateVO);
			 stateVO=(StateVO)lss.get(0);
			 
			 return new ModelAndView ("State","key",stateVO).addObject("countrylist",ls);
	   	}
	 
	 
}
