package com.Controller;

import com.DAO.CountryDAO;
import com.VO.CountryVO;
import com.sun.research.ws.wadl.Request;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller

public class CountryController {
	
	@Autowired
	CountryDAO d1;

	@RequestMapping(value="load.html" ,method=RequestMethod.GET)
	public ModelAndView loadCountry()
	{
		
		return new ModelAndView("Country","data",new CountryVO());	
	
	}
	
	@RequestMapping(value="insert.html",method=RequestMethod.POST)
	public ModelAndView Insert(@ModelAttribute CountryVO countryVO)
	{
		
		d1.insert(countryVO);
		return new ModelAndView("redirect:/load.html");
		
	}
	
	 @RequestMapping(value="search.html",method=RequestMethod.GET)
	    public ModelAndView Search(@ModelAttribute CountryVO countryVO)
	    {
	    	List searchList=new ArrayList<>();
	    	searchList=d1.search();
			return new ModelAndView("SearchCountry","SearchList", searchList);
	    }
	
	
}
