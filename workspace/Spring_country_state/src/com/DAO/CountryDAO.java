package com.DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.VO.CountryVO;


@Repository
public class CountryDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	
	public void insert(CountryVO countryVO) {
		
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.save(countryVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
	}


	public List search() {
		List searchList=new ArrayList<>();
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			Query q =session.createQuery("from CountryVO");
			searchList=q.list();
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		return searchList;
	}

}
