package com.DAO;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.VO.StateVO;

@Repository

public class StateDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public void insert(StateVO stateVO) {
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.saveOrUpdate(stateVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
	}

	public List search() {
		List searchList=new ArrayList<>();
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			Query q =session.createQuery("from StateVO");
			searchList=q.list();
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		return searchList;
		
	}

	public void delete(StateVO stateVO) {
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			session.delete(stateVO);
			
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
	}

	public List edit(StateVO stateVO) {
		List ls=new ArrayList();
		try {
			
			Session session = sessionFactory.openSession();
			
			Transaction transaction=session.beginTransaction();
			
			Query q=session.createQuery("from StateVO where id="+stateVO.getSid());
		    ls=q.list();
			transaction.commit();
			
			session.close();

			
		} 
		catch (Exception e)
		{
				e.printStackTrace();
		}
		
		return ls;
	}

}
