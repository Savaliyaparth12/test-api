package com.Controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.VO.RegVO;
import com.sun.research.ws.wadl.Request;

@Controller

public class DemoController 
{
	
	String s  ="this is spring examplle";
	
	
	@RequestMapping(value="Register.html",method=RequestMethod.GET)
	
	public ModelAndView reg()
	{
		
		return new ModelAndView("demo","key",new RegVO());
	}
	
	@RequestMapping(value="save.html",method=RequestMethod.POST)
	public ModelAndView save(@ModelAttribute RegVO v)
	{
		
		v.getFn();
		v.getLn();
		v.getUn();
		v.getPw();
		return new ModelAndView("print","data",v);
		
	}
	
	

}
