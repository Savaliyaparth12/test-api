package com.Controller;
import com.DAO.RegDAO;
import com.VO.RegVO;
import com.sun.research.ws.wadl.Request;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegController {

	@Autowired
	RegDAO d1;
	
	@RequestMapping(value="load.html" ,method=RequestMethod.GET)
	public ModelAndView load()
	{
		
		return new ModelAndView("Registration","data",new RegVO());	
	
	}
	
	@RequestMapping(value="insert.html",method=RequestMethod.POST)
	public ModelAndView Insert(@ModelAttribute RegVO regVo)
	{
		
		d1.insert(regVo);
		return new ModelAndView("redirect:/load.html");
		
	}
	
	@RequestMapping(value="search.html",method=RequestMethod.GET)
	public ModelAndView Search(@ModelAttribute RegVO regVo)
	{
		List searchList=new ArrayList<>();
		searchList=d1.search();
		return new ModelAndView("Search","SearchList", searchList);
		
	}
	
	@RequestMapping(value="delete.html",method=RequestMethod.GET)
	public ModelAndView Delete(@ModelAttribute RegVO regVo,@RequestParam int id)
	{
		regVo.setId(id);
		d1.delete(regVo);
		return new ModelAndView("redirect:/search.html");
	}
	
	@RequestMapping(value="edit.html",method=RequestMethod.GET)
	public ModelAndView Edit(@ModelAttribute RegVO regVo,@RequestParam int id)
	{
		regVo.setId(id);
		List ls=d1.edit(regVo);
		 regVo=(RegVO)ls.get(0);
		 
         return new ModelAndView ("Registration","data",regVo);
	}
	
	
}
