package com.DAO;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import com.VO.CountryVO;

public class CountryDAO {
	
	public void insert(CountryVO countryVO)
	{
	SessionFactory sessionFactory=new AnnotationConfiguration().configure().buildSessionFactory();
	Session session=sessionFactory.openSession();
	Transaction transaction=session.beginTransaction();
	
	session.save(countryVO);
	transaction.commit();
	session.close();
	
	}
	
	public List searchCountry()
	{
		List searchList=new ArrayList<>();
		
		SessionFactory sessionFactory=new AnnotationConfiguration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		Query q =session.createQuery("from CountryVO");
		searchList=q.list();
		
		transaction.commit();
		session.close();
		return searchList;
		
		
	}

}
