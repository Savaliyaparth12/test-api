package com.DAO;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import com.VO.CountryVO;
import com.VO.StateVO;

public class StateDAO {

	public void insert(StateVO stateVO)
	{
		
	try {
		SessionFactory sessionFactory=new AnnotationConfiguration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		session.save(stateVO);
		transaction.commit();
		session.close();
		} 
	catch (Exception e) 
		{
		// TODO: handle exception
		System.out.println(e);
		}
	
	}
	
	public List searchState()
	{
		List searchList=new ArrayList<>();
		try {
			SessionFactory sessionFactory=new AnnotationConfiguration().configure().buildSessionFactory();
			Session session=sessionFactory.openSession();
			Transaction transaction=session.beginTransaction();
			
			Query q =session.createQuery("from StateVO");
			searchList=q.list();
			
			transaction.commit();

			session.close();
			return searchList;
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return searchList;
	}

	public List edit(StateVO stateVO) {
		
		List ls=new ArrayList();
		SessionFactory sessionFactory=new AnnotationConfiguration().configure().buildSessionFactory();
		Session session=sessionFactory.openSession();
		Transaction transaction=session.beginTransaction();
		
		Query q = session.createQuery("from StateVO where Id ='"+stateVO.getSid()+"'");
		ls=q.list();
		transaction.commit();
		session.close();
		
		return ls;
	}

	public void update(StateVO stateVO) {
		
		SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		session.update(stateVO);
	
		
		transaction.commit();
		session.close();
		
	}
	
	
	
}
