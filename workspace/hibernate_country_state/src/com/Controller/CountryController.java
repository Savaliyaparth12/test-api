package com.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.DAO.CountryDAO;
import com.VO.CountryVO;

/**
 * Servlet implementation class CountryController
 */
@WebServlet("/CountryController")
public class CountryController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CountryController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String flag=request.getParameter("flag");
		if(flag.equals("search"))
		{	
			search(request,response);
			response.sendRedirect("SearchCountry.jsp");
		}
		
		if(flag.equals("countrylist"))
		{	
			search(request,response);
			response.sendRedirect("State.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		String flag=request.getParameter("flag");
		if(flag.equals("insert"))
		{	
			insert(request,response);
			response.sendRedirect("Country.jsp");
		}
		
	}
	protected void insert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
	
		String Cn = request.getParameter("country");
		
		CountryVO countryVO = new CountryVO();
		countryVO.setCountry(Cn);
		
		CountryDAO countryDAO = new CountryDAO();
		countryDAO.insert(countryVO);

	
    }
	
	protected void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List SearchList =new ArrayList();
		
		CountryDAO countryDAO = new CountryDAO();
		SearchList =  countryDAO.searchCountry();
		
		System.out.println("CountryCONTROLLERRRR:::::::::::::::::::"+SearchList.size());
		
		
		HttpSession session = request.getSession();
		session.setAttribute("SearchList", SearchList);
				
		
	}

}
