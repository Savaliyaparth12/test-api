<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
	<table border="2">
		<tr>
			<td>ID</td>
			<td>state</td>
			<td>Country</td>
			<td>Action</td>
		</tr>

		<c:forEach items="${sessionScope.Searchlist}" var="i">

			<tr>
				<td>${i.sid}</td>
				<td>${i.state}</td>
				<td>${i.countryVO.country}</td>
				<td><a
					href="StateController?flag=edit&id=${i.sid}&country=${i.countryVO.country}">edit</a></td>
			</tr>

		</c:forEach>
	</table>
</body>
</html>