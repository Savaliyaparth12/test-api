package com.VO;
import javax.persistence.*;

@Entity
@Table(name="Hb_fr")

public class RegVO {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="Id")
	private int Id;
	
	@Column(name="frisrtName")
	private String fristName;

	@Column(name="lastName")
	private String lastName;
	
	@ManyToOne
	private LoginVO lid;
	
	

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public String getFristName() {
		return fristName;
	}

	public void setFristName(String fristName) {
		this.fristName = fristName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LoginVO getLid() {
		return lid;
	}

	public void setLid(LoginVO lid) {
		this.lid = lid;
	}
}
