package com.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.DAO.LoginDAO;
import com.DAO.RegDAO;
import com.VO.LoginVO;
import com.VO.RegVO;

/**
 * Servlet implementation class RegController
 */
@WebServlet("/RegController")
public class RegController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String flag=request.getParameter("flag");
		if(flag.equals("search"))
		{	
			search(request,response);
			response.sendRedirect("Search.jsp");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		String flag=request.getParameter("flag");
		if(flag.equals("insert"))
		{	
			insert(request,response);
			response.sendRedirect("Register.jsp");
		}
		
	
	}
	
	protected void insert(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String Fn = request.getParameter("firstName");
		String Ln = request.getParameter("lastName");
		String Un = request.getParameter("username");
		String Pw = request.getParameter("password");
		
		LoginVO loginVO=new LoginVO();
		loginVO.setUserName(Un);
		loginVO.setPassword(Pw);
		
		LoginDAO loginDAO=new LoginDAO();
		loginDAO.insert(loginVO);
		
		RegVO regVO = new RegVO();
		regVO.setFristName(Fn);
		regVO.setLastName(Ln);
		regVO.setLid(loginVO);
		
		
		RegDAO regDAO = new RegDAO();
		regDAO.insert(regVO);

		
	}
	
	protected void search(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List SearchList =new ArrayList();
		
		System.out.println("search controlleerrrrrr");
		
		LoginDAO loginDAO=new LoginDAO();
		
		RegDAO regDAO = new RegDAO();
		SearchList =  regDAO.search();
	
		
		
		System.out.println(SearchList.size());
		
		HttpSession session = request.getSession();
		session.setAttribute("SearchList", SearchList);
				
		
	}

}
