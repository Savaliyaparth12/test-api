package com.DAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import com.VO.LoginVO;
public class LoginDAO
{

	public void insert(LoginVO loginVO)
	{
	SessionFactory sessionFactory=new AnnotationConfiguration().configure().buildSessionFactory();
	Session session=sessionFactory.openSession();
	Transaction transaction=session.beginTransaction();
	
	session.save(loginVO);
	transaction.commit();
	session.close();
	
	}
	
}
